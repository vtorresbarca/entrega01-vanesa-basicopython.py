#Script en python que emula el comportamiento del comando fdisk
#1. Mensaje de bienvenida
print("Welcome to fdisk (util-linux 2.27.1).\nChanges will remain in memory only, until you decide to write them.\nBe careful before using the write command.")

#defino las funciones
m = '''
Orden (m para obtener ayuda): m

Help:

  DOS (MBR)
   a   toggle a bootable flag
   b   edit nested BSD disklabel
   c   toggle the dos compatibility flag

  Generic
   d   delete a partition
   F   list free unpartitioned space
   l   list known partition types
   n   add a new partition
   p   print the partition table
   t   change a partition type
   v   verify the partition table
   i   print information about a partition

  Misc
   m   print this menu
   u   change display/entry units
   x   extra functionality (experts only)

  Script
   I   load disk layout from sfdisk script file
   O   dump disk layout to sfdisk script file

  Save & Exit
   w   write table to disk and exit
   q   quit without saving changes

  Create a new label
   g   create a new empty GPT partition table
   G   create a new empty SGI (IRIX) partition table
   o   create a new empty DOS partition table
   s   create a new empty Sun partition table
   '''
def menuHelp (m):
    print(m)

l = '''
0  Vacía           24  DOS de NEC      81  Minix / Linux a bf  Solaris
 1  FAT12           27  WinRE NTFS ocul 82  Linux swap / So c1  DRDOS/sec (FAT-
 2  XENIX root      39  Plan 9          83  Linux           c4  DRDOS/sec (FAT-
 3  XENIX usr       3c  PartitionMagic  84  OS/2 hidden or  c6  DRDOS/sec (FAT-
 4  FAT16 <32M      40  Venix 80286     85  Linux extendida c7  Syrinx
 5  Extendida       41  PPC PReP Boot   86  Conjunto de vol da  Datos sin SF
 6  FAT16           42  SFS             87  Conjunto de vol db  CP/M / CTOS / .
 7  HPFS/NTFS/exFAT 4d  QNX4.x          88  Linux plaintext de  Utilidad Dell
 8  AIX             4e  QNX4.x segunda  8e  Linux LVM       df  BootIt
 9  AIX arrancable  4f  QNX4.x tercera  93  Amoeba          e1  DOS access
 a  Gestor de arran 50  OnTrack DM      94  Amoeba BBT      e3  DOS R/O
 b  W95 FAT32       51  OnTrack DM6 Aux 9f  BSD/OS          e4  SpeedStor
 c  W95 FAT32 (LBA) 52  CP/M            a0  Hibernación de  ea  Rufus alignment
 e  W95 FAT16 (LBA) 53  OnTrack DM6 Aux a5  FreeBSD         eb  BeOS fs
 f  W95 Ext'd (LBA) 54  OnTrackDM6      a6  OpenBSD         ee  GPT
10  OPUS            55  EZ-Drive        a7  NeXTSTEP        ef  EFI (FAT-12/16/
11  FAT12 oculta    56  Golden Bow      a8  UFS de Darwin   f0  inicio Linux/PA
12  Compaq diagnost 5c  Priam Edisk     a9  NetBSD          f1  SpeedStor
14  FAT16 oculta <3 61  SpeedStor       ab  arranque de Dar f4  SpeedStor
16  FAT16 oculta    63  GNU HURD o SysV af  HFS / HFS+      f2  DOS secondary
17  HPFS/NTFS ocult 64  Novell Netware  b7  BSDI fs         fb  VMFS de VMware
18  SmartSleep de A 65  Novell Netware  b8  BSDI swap       fc  VMKCORE de VMwa
1b  FAT32 de W95 oc 70  DiskSecure Mult bb  Boot Wizard hid fd  Linux raid auto
1c  FAT32 de W95 (L 75  PC/IX           bc  Acronis FAT32 L fe  LANstep
1e  FAT16 de W95 (L 80  Minix antiguo   be  arranque de Sol ff  BBT
'''
def orden_l (l):
    print(l)
    
#2. Emulo las posibles opciones del comando fdisk
#2.1. Orden m
while True:
    orden = input("Orden (m para obtener ayuda): ")
    if orden == "m":
        menuHelp (m)

#2.2. Orden a        
    elif orden == "a":
        a = int(input("Número de partición (1,2,5, default 5): "))
        if a == 1:
             print("The bootable flag on partition 1 is disabled now.")
        elif a == 2:
             print("Partition 2: is an extended partition.\nThe bootable flag on partition 2 is enabled now.")
        elif a >= 3 and a < 5:
             print("The bootable flag on partition",a,"is enabled now")
        elif a == 5 or " ":
             print("The bootable flag on partition 5 is enabled now")
        else:
             print("Value out of range.")
#2.3. Orden b
    elif orden == "b":
        print("There is no *BSD partition on /dev/sda.")
        print("The device(null) does not contain BSD disklabel.")
        respuesta = str(input("Do you want to create a BSD disklabel? [Y]es/[N]o: "))
        if respuesta == "Y":
            print("There is no *BSD partition on /dev/sda.")
                
        elif respuesta == "y":
            print("There is no *BSD partition on /dev/sda.")
                
        elif respuesta == "N"or "n":
            print()          
                  
#2.4. Orden c
    elif orden == "c":
        print("DOS Compatibility flag is set (DEPRECATED!)")
#2.5. Orden d
    elif orden == "d":
        d = int(input("Número de partición(1,2,5, default 5): "))
        if d == 1:
            print("La partición",d,"ha sido eliminada.")
        elif d == 2:
            print("La partición",d,"ha sido eliminada.")
        elif d == 5 or " ":
            print("La partición 5 ha sido eliminada.") 
        else:
            print("Value out of range.")

#2.6. Orden F
    elif orden == "F":
        print('''
Unpartitioned space /dev/sda: 10 GiB, 10736369664 bytes, 20969472 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes

Start    Final Sectores Size
 2048 20971519 20969472  10G
''')
#2.7. Orden l
    elif orden == "l":
        orden_l (l)
#2.8. Orden n
    elif orden == "n":
        print('''
Partition type
   p   primary (0 primary, 0 extended, 4 free)
   e   extended (container for logical partitions)
''')
        n = str(input("Select (default p): "))
        if n == "p" or " ":
            p = input("Número de partición (2-4, default 2): ")
            if p == 2 or " ":
                p2 = input("First sector (1789950-20971519, default 17899520): ")
                if input:
                    print("First sector, +sectors or +size{K,M,G,T,P} (17899520-20971519, default 20971519): ")
                    break
            elif p == 3 or 4:
                p3 = input("First sector (1789950-20971519, default 17899520): ")
                if input:
                    print("Last sector, +sectors or +size{K,M,G,T,P} (17899520-20971519, default 20971519): ")
                    break
            else:
                print("Value out of range.")
        elif n == "e":
            e = input("Número de partición (2-4, default 2): ")
            if e == 2 or " ":
                e2 = input("First sector (1789950-20971519, default 17899520): ")
                if input:
                    print("First sector, +sectors or +size{K,M,G,T,P} (17899520-20971519, default 20971519): ")
                    break
            elif e == 3 or 4:
                e3 = input("Last sector (1789950-20971519, default 17899520): ")
                if input:
                    print("Last sector, +sectors or +size{K,M,G,T,P} (17899520-20971519, default 20971519): ")
                    break
            else:
                print("Value out of range.")
            
#2.9 Orden p
    elif orden == "p":
        print('''
Disk /dev/sda: 10 GiB, 10737418240 bytes, 20971520 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x5b59313a

Disposit.  Inicio    Start    Final Sectores  Size Id Tipo
/dev/sda2         18876414 20969471  2093058 1022M  5 Extendida
/dev/sda5         18876416 20969471  2093056 1022M 82 Linux swap / Solaris
''')
#2.10 Orden t
    elif orden == "t":
        numeroParticion = int(input("Número de partición (2,5, default 5): "))
        if numeroParticion == 3:
            print("Partition",numeroParticion,"does not exist yet!")
        elif numeroParticion == 4:
            print("Partition",numeroParticion,"does not exist yet!")
        elif numeroParticion == 2:
            tipos = input("Partition type (type L to list all types): ")
            if tipos == "L":
                orden_l(l)
        elif numeroParticion == 5 or " ":
            tipos = input("Partition type (type L to list all types): ")
            if tipos == "L":
                orden_l(l)
        else:
            print("Value out of range.")
#2.11 Orden v
    elif orden == "v":
        print("Remaining 18878462 unallocated 512-byte sectors.")
#2.12 Orden i
    elif orden == "i":
        particion = input("Número de partición (1-3, default 3): ")
        if particion == 1:
                print('''
         Device: /dev/sda1
          Start: 0
            End: 20868434
        Sectors: 20868435
      Cylinders: 1300
           Size: 10G
             Id: 83
           Type: Linux native
''')
        elif particion == 2:
                print('''
         Device: /dev/sda2
          Start: 20868435
            End: 20964824
        Sectors: 96390
      Cylinders: 7
           Size: 47,1M
             Id: 82
           Type: Linux swap
          Flags: u
''')
        elif particion == 3 or " ":
                print('''
         Device: /dev/sda3
          Start: 0
            End: 20964824
        Sectors: 20964825
      Cylinders: 1306
           Size: 10G
             Id: 5
           Type: Disco completo
''')
        else:
                print("Value out of range.")
#2.13 Orden u
    elif orden == "u":
        print("Changing display/entry units to cylinders (DEPRECATED!).")
#2.14 Orden x
    elif orden == "x":
        print("Orden avanzada")
#2.15 Orden I
    elif orden == "I":
        fileName = input("Enter script file name:")
        if input:
            print('''
Created a new DOS disklabel with disk identifier 0x5b59313a.
Created a new partition 1 of type 'Linux' and of size 9 GiB.
Created a new partition 2 of type 'Extended' and of size 1022 MiB.
Created a new partition 5 of type 'Linux swap / Solaris' and of size 1022 MiB.
Script successfully applied.
''')
#2.16 Orden O
    elif orden == "O":
        script_fileName = input("Enter script file name:")
        if input:
            print("Script successfully saved.")
#2.17 Orden w
    elif orden == "w":
        print('''
The partition table has been altered.
Calling ioctl() to re-read partition table.
Re-reading the partition table failed.: Dispositivo o recurso ocupado

The kernel still uses the old table. The new table will be used at the next reboot or after you run partprobe(8) or kpartx(8).
''')
        break
#2.18 Orden g
    elif orden == "g":
        print("Created a new GPT disklabel (GUID: 34A3EDBE-E6D2-4DDC-8EB0-548266F806AC).")
#2.19 Orden G
    elif orden == "G":
        print('''
Created a new partition 11 of type 'SGI volume' and of size 10 GiB.
Created a new partition 9 of type 'SGI volhdr' and of size 2 MiB.
Created a new SGI disklabel.
''')
#2.20 Orden o
    elif orden == "o":
        print("Created a new DOS disklabel with disk identifier 0x255834b5.")
#2.21 Orden s
    elif orden == "s":
        print('''
Created a new partition 1 of type 'Linux native' and of size 10 GiB.
Created a new partition 2 of type 'Linux swap' and of size 47,1 MiB.
Created a new partition 3 of type 'Whole disk' and of size 10 GiB.
Created a new Sun disklabel.
''')
#2.22 Orden q
    elif orden == "q":
        break


        



        



